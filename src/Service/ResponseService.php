<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class ResponseService {
    
    public function getResponceObject($message, $data)
    {
        return [
            'success' => $data ? true : false,
            'data' => $data,
            'message'=> $message,
        ];
    }
}