<?php

namespace App\Service;

use \SwiftMailer;

class MailService {

    public function sendEmail(string $name, string $email, string $subject, string $message)
    {
        $this->send("Control Application :: Сообщение с контактной формы,
        <p style='color:red;'>Пользователь: <span style='color:black;'>{$name} || {$email} </span></p>
        <p style='color:red;'>Тема: <span style='color:black;'>{$subject}</span></p>
        <p style='color:red;'>Сообщение: <span style='color:black;'>{$message}</span></p>");
    }

    public function sendRecall(string $name, string $phoneNumber)
    {
        $this->send("Control Application :: Заказ на звонок,
        <p style='color:red;'>Пользователь: <span style='color:black;'>{$name} </span></p>
        <p style='color:red;'>Просит перезвонить на номер: <span style='color:black;'>{$phoneNumber}</span></p>");
    }

    private function send($body)
    {
        $transport = (new \Swift_SmtpTransport('smtp.googlemail.com', 465, 'ssl'))
            ->setUsername('tthwacka')
            ->setPassword('01031996412369')
        ;

        $mailer = (new \Swift_Mailer($transport));

        $message = (new \Swift_Message('Control Application'))
          ->setFrom(['npswim@controlpanel.com' => 'EMAIL_CONTACT'])
          ->setTo(['Sport_i_vy@mail.ru'])
          ->setBody($body)
          ->setContentType('text/html')
      ;

      $mailer->send($message);
    }
}