<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AuthLoggerRepository")
 */
class AuthLogger
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sessionId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateLogin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateLogout;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    public function __construct(\App\Entity\User $user, string $sessionId)
    {
        $this->user = $user;
        $this->sessionId = $sessionId;
        $this->dateLogin = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(?string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getDateLogin(): ?\DateTimeInterface
    {
        return $this->dateLogin;
    }

    public function setDateLogin(?\DateTimeInterface $dateLogin): self
    {
        $this->dateLogin = $dateLogin;

        return $this;
    }

    public function getDateLogout(): ?\DateTimeInterface
    {
        return $this->dateLogout;
    }

    public function setDateLogout(?\DateTimeInterface $dateLogout): self
    {
        $this->dateLogout = $dateLogout;

        return $this;
    }

    public function setUser(?\App\Entity\User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?\App\Entity\User
    {
        return $this->user;
    }
}
