<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Claim;

use App\Service\ResponseService;
use App\Service\MailService;

class ClaimController extends AbstractController
{
    /**
     * @Route("/claim/create/mail", methods={"POST"}, name="create_mail_claim")
     */
    public function indexContact(Request $request ,ResponseService $rs, MailService $ms)
    {
        $ms->sendEmail($request->get('name'), $request->get('email'), $request->get('subject'), $request->get('message'));

        $entityManager = $this->getDoctrine()->getManager();

        $claim = new Claim();

        $claim->setName($request->get('name'));
        $claim->setEmail($request->get('email'));
        $claim->setSubject($request->get('subject'));
        $claim->setMessage($request->get('message'));
        $claim->setStatus(false);

        $entityManager->persist($claim);
        $entityManager->flush();

        return $this->json($rs->getResponceObject('MESSAGE_SEND', true));
    }

    /**
     * @Route("/claim/create/phone", methods={"POST"}, name="create_phone_claim")
     */
    public function indexPhone(Request $request ,ResponseService $rs, MailService $ms)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $ms->sendRecall($request->get('name'), $request->get('phoneNumber'));

        $claim= new Claim();

        $claim->setName($request->get('name'));
        $claim->setPhoneNumber($request->get('phoneNumber'));
        $claim->setStatus(false);

        $entityManager->persist($claim);
        $entityManager->flush();

        return $this->json($rs->getResponceObject('MESSAGE_SEND', true));
    }
}
