<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use \Datetime;
use App\Entity\User;
use App\Entity\AuthLogger;
use App\Service\ResponseService;

class AuthController extends AbstractController
{
    /**
     * @Route("/login", methods={"POST"}, name="login")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder, ResponseService $rs)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $encodedPassword = $encoder->encodePassword(new User(), $request->get('password'));

      $user = $entityManager->getRepository(User::class)->findOneBy([
         'username' => $request->get('username'),
        ' password' => $encodedPassword
      ]);

      if (!$user) {
         return $this->json($rs->getResponceObject('Authorization error. Please! Check input information and try again!', null));
      }
      
      if (!$user->getActivateUser()) {
          return $this->json($rs->getResponceObject('Account not activated. Please! Check your email address.', null));
      }
  
      $token = $this->generateToken($user, $encoder);
      $authLogger = new AuthLogger($user, $token['sessionId']);
			
			$user->setAccessToken($token['accessToken']);
			$user->setAccessExpire($token['accessExpire']);
			$user->setRefreshToken($token['refreshToken']);
			$user->setRefreshExpire($token['refreshExpire']);
      $entityManager->persist($authLogger);
			$entityManager->flush();

      return $this->json($rs->getResponceObject('AUTHENTICATION_SUCCESS',[
          'firstname' => $user->getFirstname(),
          'secondname' => $user->getLastname(),
          'cliendId' => $user->getId(),
          'accessToken' => $token['accessToken'],
          'refreshToken' => $token['refreshToken'],
          'sessionId' => $token['sessionId'],
      ]));
    }

    private function generateToken(User $user, UserPasswordEncoderInterface $encoder)
    {
      return [
          'accessToken' => $encoder->encodePassword($user, date("Y-m-d H:i:s")),
          'accessExpire' => (strtotime(date("Y-m-d H:i:s")) * 1000) + 604800000,
          'refreshToken' => $encoder->encodePassword($user, strtotime(date("Y-m-d H:i:s")) * 1000),
          'refreshExpire' => (strtotime(date("Y-m-d H:i:s")) * 1000) + 259200000,
          'sessionId' => $encoder->encodePassword($user, strtotime(date("Y-m-d H:i:s")) * 1500)
      ];
    }

    /**
     * @Route("/logout", methods={"POST"}, name="logout")
     */
    public function logout(Request $request, ResponseService $rs)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $authLog = $entityManager->getRepository(AuthLogger::class)->findOneBy([
            'sessionId' => $request->get('sessionId'),
        ]);

        if (!$authLog || ($authLog && $authLog->getDateLogout())) {
            return $this->json($rs->getResponceObject('LOGOUT_ERROR', null));
        }

        $authLog->setDateLogout(new \DateTime());
        $entityManager->flush();
        return $this->json($rs->getResponceObject('LOGOUT_SUCCESS', true));
    }

    /**
     * @Route("/registration", methods={"POST"}, name="registration")
     */
    public function registration(Request $request, UserPasswordEncoderInterface $encoder, ResponseService $rs)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $registredUser = $entityManager->getRepository(User::class)->findByUsernameOrEmail(
            $request->get('username'),
            $request->get('emailAddress')
        );

        if ($registredUser) {
            return $this->json($rs->getResponceObject('DATA_ARLEADY_EXIST', null));
        }

        $user = new User();

        $user->setUsername($request->get('username'));
        $user->setPassword($encoder->encodePassword($user, $request->get('password')));
        $user->setEmailAddress($request->get('emailAddress'));
        $user->setActivateUser(false);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json($rs->getResponceObject('USER_WAS_CREATED', true));
    }
}
