<?php

namespace App\Repository;

use App\Entity\AuthLogger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AuthLogger|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthLogger|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthLogger[]    findAll()
 * @method AuthLogger[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthLoggerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AuthLogger::class);
    }

//    /**
//     * @return AuthLogger[] Returns an array of AuthLogger objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AuthLogger
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
