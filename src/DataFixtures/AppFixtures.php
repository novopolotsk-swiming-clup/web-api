<?php

namespace App\DataFixtures;
use App\Entity\User;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadRoles();
        $this->loadUsers();
        $manager->flush();
    }

    public function loadRoles()
    {
        $role = new Role();
        $role->setName('ROLE_ADMIN');
        $manager->persist($role);

        $role = new Role();
        $role->setName('ROLE_USER');
        $manager->persist($role);
    }

    public function loadUsers()
    {
        $user = new User();
        $user->setUsername('thwacka');
        $user->setPassword('BFEQkknI\\/c+Nd7BaG7AaiyTfUFby\\/pkMHy3UsYqKqDcmvHoPRX\\/ame9TnVuOV2GrBH0JK9g4koW+CgTYI9mK+w==');
        $user->setFirstname('Test');
        $user->setLastname('User');
        $user->setEmailAddress('admin@test.local');
        $user->setRole(1);

        $manager->persist($user);
    }
}
